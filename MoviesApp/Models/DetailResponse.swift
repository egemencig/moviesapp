//
//  DetailResponse.swift
//  MoviesApp
//
//  Created by Egemen Çığ on 21.03.2021.
//

import Foundation
import ObjectMapper
import RealmSwift

class DetailResponse: BaseResponseModel {
    
    var poster_path: String?
    var original_title: String?
    var overview: String?
    var vote_count: Int?
    var release_date: String?
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        poster_path <- map["poster_path"]
        original_title <- map["original_title"]
        overview <- map["overview"]
        vote_count <- map["vote_count"]
        release_date <- map["release_date"]
    }
}
