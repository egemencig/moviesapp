//
//  StoreManager.swift
//  VPN - JRSnet
//
//  Created by Egemen Çığ on 22.03.2021.
//  Copyright © 2021 jrsworld. All rights reserved.
//

import Foundation
import UIKit

class StoreManager {
    
    static let shared = StoreManager()

    
    private static func storeFinalLocation(data : String) -> NSData {

        return NSKeyedArchiver.archivedData(withRootObject: data as String) as NSData
    }
    
    private static func storePremium(data : Bool) -> NSData {

        return NSKeyedArchiver.archivedData(withRootObject: data as Bool) as NSData
    }

    
     func loadFinalLocation() -> String? {

        if let unarchivedObject = UserDefaults.standard.object(forKey: "finalLocation") as? Data {

            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? String
        }

        return nil
    }
    
     func saveFinalLocation(data : String?) {
        
        let archivedObject = StoreManager.storeFinalLocation(data: data!)
        UserDefaults.standard.set(archivedObject, forKey: "finalLocation")
        UserDefaults.standard.synchronize()
    }
    
    func getFinalLocation() -> String {
        var finalLocation: String = String()
        if StoreManager.shared.loadFinalLocation() == nil {
            StoreManager.shared.saveFinalLocation(data: finalLocation)
        }
        else {
            finalLocation = StoreManager.shared.loadFinalLocation() ?? ""
        }
        return finalLocation
    }
    
    func loadIsPremium() -> Bool? {

       if let unarchivedObject = UserDefaults.standard.object(forKey: "isPremium") as? Data {

           return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? Bool
       }

       return nil
   }
   
    func saveIsPremium(data : Bool?) {
       
       let archivedObject = StoreManager.storePremium(data: data!)
       UserDefaults.standard.set(archivedObject, forKey: "isPremium")
       UserDefaults.standard.synchronize()
   }
   
   func getIsPremium() -> Bool {
       var isPremium: Bool = Bool()
       if StoreManager.shared.loadIsPremium() == nil {
           StoreManager.shared.saveIsPremium(data: isPremium)
       }
       else {
        isPremium = StoreManager.shared.loadIsPremium() ?? false
       }
       return isPremium
   }

}
