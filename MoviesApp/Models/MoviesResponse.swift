//
//  MoviesResponse.swift
//  MoviesApp
//
//  Created by Egemen Çığ on 21.03.2021.
//

import Foundation
import ObjectMapper
import RealmSwift

class MoviesResponse: BaseResponseModel {
    
    var page : Int?
    var results : [ResultModel]?
    var total_pages : Int?
    var total_results : Int?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        page <- map["page"]
        results <- map["results"]
        total_pages <- map["total_pages"]
        total_results <- map["total_results"]
    }
}

