//
//  LikeModel.swift
//  MoviesApp
//
//  Created by Egemen Çığ on 21.03.2021.
//

import Foundation

class LikeModel:NSObject, NSCoding  {
    
    var movieId: String
    
    required init(movieId: String) {
        self.movieId = movieId
    }
    
    required init(coder aDecoder: NSCoder) {
        movieId = (aDecoder.decodeObject(forKey: "movieId")) as! String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(movieId, forKey: "movieId")
    }
}

