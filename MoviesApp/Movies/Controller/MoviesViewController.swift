//
//  MoviesViewController.swift
//  MoviesApp
//
//  Created by Egemen Çığ on 21.03.2021.
//

import UIKit

class MoviesViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var viewModel: MoviesViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestStatus = .pending
        viewModel.delegate = self
        viewModel.setupCollectionView()
        viewModel.getMovies()
        setupNavigationBar()
        setSearchBar()
        setFloatingButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.likesArray = StoreManager.shared.getLikesArray()
        viewModel.collectionView.reloadData()
    }
    
    func setSearchBar() {
        viewModel.searchController.searchBar.searchTextField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 175/255, green: 175/255, blue: 175/255, alpha: 1.0), NSAttributedString.Key.font : UIFont(name: "HelveticaNeue-Medium", size: 13.0)!])
        viewModel.searchController.searchBar.searchTextField.textColor = .black
        viewModel.searchController.searchBar.searchTextField.backgroundColor = .clear
        viewModel.searchController.searchBar.searchTextField.font = UIFont(name: "HelveticaNeue-Medium", size: 15.0)
        viewModel.searchController.searchBar.sizeToFit()
        viewModel.searchController.searchBar.tintColor = .black
        viewModel.searchController.hidesNavigationBarDuringPresentation = false
        viewModel.searchController.searchBar.barStyle = .default
        viewModel.searchController.dimsBackgroundDuringPresentation = false
        viewModel.searchController.searchBar.barTintColor = .white
        viewModel.searchController.searchBar.isTranslucent = true
        viewModel.searchController.searchBar.isHidden = false
        let attributes:[NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.black,
            .font: UIFont(name: "HelveticaNeue-Bold", size: 18.0)
        ]
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
        
        for view in viewModel.searchController.searchBar.subviews {
            for subview in view.subviews where subview.isKind(of: NSClassFromString("UISearchBarBackground")!) {
                subview.alpha = 0
            }
        }
    }
    
    
    @objc func upButtonTapped(_ sender: UIButton) {
        print("tapped")
        viewModel.collectionView.setContentOffset(.zero, animated: true)
        viewModel.floatingButton.isHidden = true
    }
    
    func setFloatingButton(){
        
        let window = UIApplication.shared.keyWindow
        let bottomPadding = window?.safeAreaInsets.bottom
        viewModel.floatingButton.frame = CGRect(x: self.view.frame.width - 65, y: UIScreen.main.bounds.height - bottomPadding! - 80, width: 56, height: 56)
        viewModel.floatingButton.setImage(UIImage(named: "upicon"), for: .normal)
        viewModel.floatingButton.backgroundColor = UIColor(red: 105/255, green: 74/255, blue: 254/255, alpha: 1.0)
        viewModel.floatingButton.clipsToBounds = true
        viewModel.floatingButton.layer.cornerRadius = 28
        viewModel.floatingButton.addTarget(self, action: #selector(self.upButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(viewModel.floatingButton)
    }
    
    func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.backgroundColor = UIColor.darkGray
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.setStatusBar(backgroundColor: UIColor.darkGray)
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        definesPresentationContext = true
        self.navigationItem.title = "Movies"
        self.navigationItem.searchController = viewModel.searchController
    }
    
}

extension MoviesViewController: MovieListDelegate {
    func didSelect(movieModel: ResultModel) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let viewController = mainStoryboard.instantiateViewController(withIdentifier: "DetailsViewController") as? DetailsViewController {
            if let movieId = movieModel.id {
                viewController.movieId = movieId
            }
            self.navigationController?.pushViewController(viewController, animated: false)
        }
    }
    
    func changed(status: BaseViewController.RequestStatus) {
        requestStatus = status
        switch status {
        case .completed(let error):
            if error != nil {
                print("error")
            }
            else {
                viewModel.collectionView.reloadData()
                viewModel.floatingButton.isHidden = true
            }
        default: break
        }
    }
    
}
