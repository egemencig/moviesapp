//
//  MoviesViewModel.swift
//  MoviesApp
//
//  Created by Egemen Çığ on 21.03.2021.
//

import Foundation
import UIKit

protocol MovieListDelegate {
    func changed(status: BaseViewController.RequestStatus)
    func didSelect(movieModel: ResultModel)
}

class MoviesViewModel: NSObject {
    
    @IBOutlet var collectionView: UICollectionView! = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
    let searchController = UISearchController(searchResultsController: nil)
    
    var delegate: MovieListDelegate?
    var moviesResponse: MoviesResponse?
    var moviesArray: [ResultModel] = []
    var searchResults: [ResultModel] = []
    let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
    var pageNumber = 0
    let floatingButton = UIButton()
    var likesArray = [LikeModel]()
    
    
    func setupCollectionView() {
        layout.scrollDirection = .vertical
        collectionView.register(UINib(nibName: "MoviesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "movieCell")
        collectionView.setCollectionViewLayout(layout, animated: true)
        collectionView.alwaysBounceVertical = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.backgroundColor = .clear
        collectionView.layer.cornerRadius = 10
        searchController.searchBar.delegate = self
        likesArray = StoreManager.shared.getLikesArray()
        collectionView.reloadData()
    }
    
    func getMovies() {
        pageNumber += 1
        API.sharedManager.getMovies(pageId: pageNumber) { (response, error) in
            if error != nil {
                print("error")
            }
            else {
                if let movieList = response?.results {
                    self.moviesArray.append(contentsOf: movieList)
                    self.delegate?.changed(status: .completed(error))
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        if currentOffset > 1000 {
            floatingButton.isHidden = false
        }
        else {
            floatingButton.isHidden = true
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if maximumOffset - currentOffset <= 10.0 {
            self.getMovies()
        }
    }
    
}

extension MoviesViewModel: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if searchController.isActive == true && searchController.searchBar.text != "" {
            self.delegate?.didSelect(movieModel: searchResults[indexPath.row])
        }
        else {
            self.delegate?.didSelect(movieModel: moviesArray[indexPath.row])
        }
    }
}

extension MoviesViewModel: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchController.isActive == true && searchController.searchBar.text != "" {
            return searchResults.count
        }
        else {
            return moviesArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MoviesCollectionViewCell
        
        cell.backgroundColor = .clear
        cell.layer.cornerRadius = 10
        
        var movieCellViewModel = MovieCellViewModel()
        if searchController.isActive == true && searchController.searchBar.text != "" {
            movieCellViewModel = MovieCellViewModel(resultModel: searchResults[indexPath.row])
        }
        else {
            movieCellViewModel = MovieCellViewModel(resultModel: moviesArray[indexPath.row])
        }
        
        cell.dataSource = movieCellViewModel
        cell.reloadData()
        cell.movieImage.backgroundColor = .red
        cell.starButton.isHidden = true
        for index in 0..<likesArray.count {
            if let index = moviesArray.firstIndex(where: {$0.id == Int(likesArray[index].movieId)}) {
                if indexPath.row == index {
                    cell.starButton.isHidden = false
                }
            }
        }
        
        return cell
    }
    
}

extension MoviesViewModel: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width - 10) / 2  , height: (collectionView.frame.size.width - 10) / 1.3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
}

extension MoviesViewModel: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        collectionView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchResults = moviesArray.filter({ (ResultModel) -> Bool in
            let match = ResultModel.original_title?.range(of: searchText, options: .caseInsensitive)
            return (match != nil)
        })
        collectionView.reloadData()
    }
    
}
