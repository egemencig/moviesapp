//
//  MovieCellViewModel.swift
//  MoviesApp
//
//  Created by Egemen Çığ on 21.03.2021.
//

import Foundation

class MovieCellViewModel: NSObject {
    var resultModel: ResultModel?
    let imageUrl = "https://image.tmdb.org/t/p/w400"
    
    init(resultModel: ResultModel = ResultModel()) {
        self.resultModel = resultModel
    }
    
}

extension MovieCellViewModel: MoviesCollectionViewCellDataSource {
    func imageForCell(_ cell: MoviesCollectionViewCell) -> URL {
        if let path = resultModel?.poster_path, let url = URL(string: imageUrl + path) {
            return url
        }
        else {
            return URL(string: "https://www.atolyesaga.com/wp-content/uploads/2018/06/image_large.png")!
        }
    }
    
    func titleForCell(_ cell: MoviesCollectionViewCell) -> String {
        if let title = resultModel?.original_title {
            return title
        }
        else {
            return ""
        }
    }
    
}
