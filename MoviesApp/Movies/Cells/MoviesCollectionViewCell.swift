//
//  MoviesCollectionViewCell.swift
//  MoviesApp
//
//  Created by Egemen Çığ on 21.03.2021.
//

import UIKit
import SDWebImage

protocol MoviesCollectionViewCellDataSource {
    func imageForCell(_ cell: MoviesCollectionViewCell) -> URL
    func titleForCell(_ cell: MoviesCollectionViewCell) -> String
}

class MoviesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var starButton: UIButton!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    var dataSource: MoviesCollectionViewCellDataSource?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        movieImage.contentMode = .scaleAspectFill
    }
    
    func reloadData() {
        movieImage.sd_setImage(with: dataSource?.imageForCell(self), placeholderImage: UIImage(named: "placeHolder"), options: SDWebImageOptions(), context: nil)
        movieTitle.text = dataSource?.titleForCell(self)
    }
    
}
