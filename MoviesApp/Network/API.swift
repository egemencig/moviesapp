//
//  API.swift
//  MoviesApp
//
//  Created by Egemen Çığ on 21.03.2021.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class API {
    
    static let sharedManager = API()
    private var sessionManager = SessionManager()
    private init() { }
    private var apiKey = "fd2b04342048fa2d5f728561866ad52a"
    

    fileprivate let encoding = JSONEncoding.default

    func getMovies(pageId: Int,completion: @escaping (MoviesResponse?, Error?) -> Void) {
        let endpoint = "https://api.themoviedb.org/3/movie/popular?language=en-US&api_key=\(apiKey)&page=\(pageId)"
        request(endpoint: .getMovies, method: .get, endpointURL: endpoint).responseObject {
            (response: DataResponse<MoviesResponse>) in self.handle(response: response, completion: completion)
        }
    }
    
    func getDetails(movieId: Int,completion: @escaping (DetailResponse?, Error?) -> Void) {
        let endpoint = "https://api.themoviedb.org/3/movie/\(movieId)?language=en-US&api_key=\(apiKey)"
        request(endpoint: .getDetails, method: .get, endpointURL: endpoint).responseObject {
            (response: DataResponse<DetailResponse>) in self.handle(response: response, completion: completion)
        }
    }
    
}

extension API {
    
    fileprivate func handle<T>(response: DataResponse<T>, completion: @escaping (T?, Error?) -> Void) {
        
        if response.error != nil {
            completion(nil, response.error)
        }
        if let responseResultValue = response.result.value {
            completion(responseResultValue, nil)
        }
        else {
            completion(nil, response.result.error)
        }
    }
}

extension API {
    
    fileprivate func request(endpoint: Endpoint,method: HTTPMethod, endpointURL: String) -> DataRequest {
        
        return sessionManager.request(endpointURL,
                                      method: method,
                                      encoding: API.sharedManager.encoding)
    }
    
    enum Endpoint {
        case getMovies
        case getDetails
    }
}

