//
//  DetailsViewController.swift
//  MoviesApp
//
//  Created by Egemen Çığ on 21.03.2021.
//

import UIKit
import SDWebImage

class DetailsViewController: BaseViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var movieId: Int = Int()
    var detailModel: DetailResponse = DetailResponse()
    let imageUrl = "https://image.tmdb.org/t/p/w400"
    var likesArray = [LikeModel]()
    let rightButton = UIButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestStatus = .pending
        getDetails()
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.largeTitleDisplayMode = .never
        self.navigationItem.title = "Details"
        setupTableView()
        likesArray = StoreManager.shared.getLikesArray()
        setNavigationBarButton()
        
    }
    
    func setNavigationBarButton() {
        
        if likesArray.contains(where: {$0.movieId == String(self.movieId)}) {
            rightButton.setImage(UIImage(named: "fillstar"), for: .normal)
        }
        else {
            rightButton.setImage(UIImage(named: "blankstar"), for: .normal)
        }
        rightButton.frame = CGRect(x: 0, y: 0, width: 45, height: 30)
        rightButton.addTarget(self, action: #selector(self.likeButtonTapped), for: .touchUpInside)
        let item = UIBarButtonItem(customView: rightButton)
        self.navigationItem.setRightBarButtonItems([item], animated: true)
    }
    
    @objc func likeButtonTapped() {
        if likesArray.contains(where: {$0.movieId == String(self.movieId)}) {
            if let index = likesArray.firstIndex(where: {$0.movieId == String(self.movieId)}) {
                likesArray.remove(at: index)
                StoreManager.shared.saveLikesArray(data: likesArray)
            }
        }
        else {
            let likeModel = LikeModel(movieId: String(self.movieId))
            likesArray.append(likeModel)
            StoreManager.shared.saveLikesArray(data: likesArray)
        }
        
        if likesArray.contains(where: {$0.movieId == String(self.movieId)}) {
            rightButton.setImage(UIImage(named: "fillstar"), for: .normal)
        }
        else {
            rightButton.setImage(UIImage(named: "blankstar"), for: .normal)
        }
    }
    
    
    func setupTableView() {
        tableView.register(UINib(nibName: "DetailTableViewCell", bundle: nil), forCellReuseIdentifier: "detailCell")
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.dataSource = self
    }
    
    func getDetails() {
        API.sharedManager.getDetails(movieId: movieId) { [self] (response, error) in
            if let movie = response {
                self.detailModel = movie
                tableView.reloadData()
                requestStatus = .completed(error)
            }
        }
    }
    
    func convertDateFormater(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMMM d, yyyy"
        return  dateFormatter.string(from: date!)
    }
    
}

extension DetailsViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! DetailTableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        if let path = detailModel.poster_path, let imageUrl = URL(string: self.imageUrl + path), let title = detailModel.original_title, let description = detailModel.overview, let voteCount = detailModel.vote_count, let release = detailModel.release_date {
            cell.movieImage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "placeHolder"), options: SDWebImageOptions(), context: nil)
            cell.titleLabel.text = title
            cell.voteLabel.text = "Votes: \(voteCount)"
            cell.descriptionLabel.text = description
            cell.releaseLabel.text = "Released Date: \(convertDateFormater(release))"
        }
        
        return cell
    }
}
