Projeyi MVVM kod şeması ile yazdım.
Uygulamada filmlerin listelendiği sayfa ve detay ekranı mevcut.
Kullanıcının favorilerine eklemek istediği filmin id’sini User Defaults’ta depoladım. Kullanıcı istediği filmi favorilerine ekleyip çıkarabilir.
Sorgu atarken parametrik olarak page number’ı kullanıcı sayfa sonuna geldiğinde arttırdım ve istediğiniz gibi anasayfada load more kullandım. Kullanıcı sayfayı kaydırdıkça filmler otomatik olarak gelecektir.
Sayfa yukarı doğru scoll edildiğinde kullanıcının sayfanın en başına dönebilmesi için bir buton ekledim.
Kullanıcı Search Bar’da istediği filmi aratıp bulabilir.
Detay ekranını olabildiğince basit tutmaya çalıştım. Tıklanılan filmin id’sini kullanıp sorgu atarak kullanıcıya detayları gösterdim.
Basit, anlaşılır ve düzenli kod yazmaya çalıştım.
